package br.com.senac.servlet;

import br.com.senac.agenda.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {

        String usuario = requisicao.getParameter("user");
        String navedor = requisicao.getHeader("User-Agent");

        // tipo da resposta 
        resposta.setContentType("text/html");
        PrintWriter out = resposta.getWriter();
        out.println("<html>");
        out.println("<body>");
        out.println("<b>Olá , " + usuario + " </b><br/>");
        out.println("Seja Bem-Vindo ao Mundo dos Servlets !!!<br/>");
        out.println("Voce esta usando o navegador:" + navedor);
        out.println("</body>");
        out.println("</html>");

    }

    @Override
    protected void doPost(HttpServletRequest requisicao, HttpServletResponse resposta) throws ServletException, IOException {

        String nome = requisicao.getParameter("usuario");
        String senha = requisicao.getParameter("senha");

        Usuario usuario = new Usuario(nome, senha);

        PrintWriter out = resposta.getWriter();
        out.println("<html>");
        out.println("<body>");
        if (usuario.getNome().equals("tiago") && usuario.getSenha().equals("123")) {
            out.println("Seja bem-vindo " + usuario.getNome());
        } else {
            out.println("Falha na autenticacao!!!");
        }
        out.println("</body>");
        out.println("</html>");

    }

}
